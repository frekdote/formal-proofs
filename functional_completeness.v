Set Warnings "-notation-overridden".
From mathcomp Require Import all_ssreflect.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

(* logical function with [n] inputs *)
Definition lfun n :=
  {ffun bool ^ n -> bool}.

Definition proj n (k : 'I_n) : lfun n :=
  [ffun a : bool ^ n => a k].

Definition lcomp m n (f : lfun m) (fs : lfun n ^ m) : lfun n :=
  [ffun a => f [ffun k => fs k a]].

Definition lnot : lfun 1 :=
  [ffun a : bool ^ 1 => negb (a ord0)].

Definition lxor : lfun 2 :=
  [ffun a : bool ^ 2 => xorb (a ord0) (a (lift ord0 ord0))].

Definition lnand : lfun 2 :=
  [ffun a : bool ^ 2 => negb (andb (a ord0) (a (lift ord0 ord0)))].

Definition one n : lfun n :=
  [ffun a => true].

Inductive is_built_from n :
  lfun n ->
  (forall m, seq (lfun m)) ->
  Prop :=
| Proj : forall k fss, is_built_from (proj k) fss
| Comp : forall m (f : lfun m) (fs : lfun n ^ m) fss,
    f \in fss m ->
    (forall k, is_built_from (fs k) fss) ->
    is_built_from (lcomp f fs) fss.

Definition is_functionally_complete (fss : forall m, seq (lfun m)) :=
  forall n (f : lfun n), is_built_from f fss.

Theorem not_is_not_functionally_complete :
  ~ is_functionally_complete (fun m => match m with 1 => [:: lnot] | _ => [::] end).
Proof.
move=> /(_ 1 (one _)) H.
have : one 1 = proj ord0 \/ one 1 = lcomp lnot (finfun_of_tuple [tuple of [:: proj ord0]]).
  move: H.
  set fss := fun _ => _.
  move: (erefl fss).
  rewrite {2}/fss => + H.
  elim: _ fss / H.
  - by move=> k fss _; left; rewrite ord1.
  - case => [| [| m]] /= f fs fss in_f _ IH Efss; rewrite Efss // in in_f.
    rewrite mem_seq1 in in_f; move: in_f => /eqP ->.
    case: (IH ord0 Efss) => /ffunP Efs0.
    + right.
      apply/ffunP => a.
      by rewrite !ffunE Efs0 ffunE.
    + left.
      apply/ffunP => a.
      by rewrite !ffunE Efs0 !ffunE negbK.
case => /ffunP.
- move=> /(_ (finfun_of_tuple [tuple of [:: false]])).
  by rewrite !ffunE tnth0.
- move=> /(_ (finfun_of_tuple [tuple of [:: true]])).
  by rewrite !ffunE tnth0.
Qed.

Theorem xor_is_not_functionally_complete :
  ~ is_functionally_complete (fun m => match m with 2 => [:: lxor] | _ => [::] end).
Proof.
Admitted.

Theorem nand_is_functionally_complete :
  is_functionally_complete (fun m => match m with 2 => [:: lnand] | _ => [::] end).
Proof.
Admitted.
